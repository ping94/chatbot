
  function generateUniqueLink(sender){
    return "https://recruitapp.site/apply?id="+ sender;
  }

  function getJobElements(){
    var elements = [{
      "title":"Cabin crew",
      "subtitle": "Description for cabin crew",
      "buttons": [
          {
            "title": "Apply",
            "type": "postback",
            "payload": "cabin crew",
          },
          {
            "title": "Learn More",
            "type": "postback",
            "payload": "cabin crew",
          }
      ],
    },{
      "title":"Pilot",
      "subtitle": "Description for pilot",
      "buttons": [
          {
            "title": "Apply",
            "type": "postback",
            "payload": "pilot",
          },
          {
            "title": "Learn More",
            "type": "postback",
            "payload": "pilot",
          }
      ],
    }];
    return elements;
  }

  module.exports = {
    generateUniqueLink : generateUniqueLink,
    getJobElements : getJobElements,
  };
