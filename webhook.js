var Util = require('./util.js');
var Database = require('./database.js');
var ACCESS_TOKEN = 'EAAUyJl7604MBAK7f8yGne6MobzSj5JH1D6unzArbl1bQDzs4N6Oaf5A6QVFJhfMF1CC9nkXiFSAZB78xeVzaNC0NxY0WXvhWAB1ZA1ADU5SDuVDnK8jzbwHZAUtBiL4RAMqx0faw8FCWraedqVttOkIQT8VLAyQiLRlJpnOcAZDZD';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const server = app.listen(process.env.PORT || 5000, () => {
  console.log('Express server listening on port %d in %s mode', server.address().port, app.settings.env);
});

Database.startConnection();

/* For Facebook Validation */
app.get('/webhook', (req, res) => {
  if (req.query['hub.mode'] && req.query['hub.verify_token'] === 'tuxedo_cat') {
    res.status(200).send(req.query['hub.challenge']);
  } else {
    res.status(403).end();
  }
});

/* Handling all messenges */
app.post('/webhook', (req, res) => {
  console.log(req.body);
  if (req.body.object === 'page') {
    req.body.entry.forEach((entry) => {
      entry.messaging.forEach((event) => {
        var text;
        let sender = event.sender.id;
        console.log("SENDER ID = " + sender);

        if (event.message && event.message.text) {
          text = event.message.text;
          sendMessage(sender,text);
        }

        if (event.postback) {
          text = JSON.stringify(event.postback);

         if(event.postback.title == "Apply"){
            doApply(sender,event.postback.payload);
         }

         if(event.postback.title == "Learn More"){
            doLearnMore(sender,event.postback.payload);
         }
          console.log("TEXT = " + text);
       }

       Database.insertMessage(sender,text,'U');
      });
    });
    res.status(200).end();
  }
});

function doApply(sender,payload){
  if(payload == "cabin crew" || payload == "flight attendant"){
    botSendTextMessage(sender,"Cool, please visit this unique link for application! At the very beginning stage, you will be asked to do a digital video interview to let us know more about yourself. It can be started anytime anywhere as long as you have a webcam! :) " + Util.generateUniqueLink(sender));
  }else{
    botSendTextMessage(sender,"Awesome! Please check out this page to see if you have met the basic requirements! https://recruitapp.site/career");
  }

}

function doLearnMore(sender,payload){
  botSendTextMessage(sender,"I will try my best to answer your questions! Or you can visit our career website to learn more about the job nature and requirements. https://www.singaporeair.com/en_UK/sg/careers/");
}

app.get('/test', (req, res) => {
  console.log(req.url)
  res.end('Hello FB!')
});

const request = require('request');
const apiaiApp = require('apiai')('935e3da64cde49cdbd8a972a3ac0e4ba');

function sendMessage(sender, text) {
  console.log("TEXT = " + text);
  let apiai = apiaiApp.textRequest(text, {
    sessionId: 'tabby_cat' // use any arbitrary id
  });

  apiai.on('response', (response) => {
    console.log("RESPONSE = " + JSON.stringify(response));
    let aiText = response.result.fulfillment.speech;

    if(response.result.action == "answer_job_opening"){
        let elements = Util.getJobElements();
        botSendTextMessage(sender, aiText);
        botSendList(sender, elements, aiText);
    }else if(response.result.action == "answer_work_environment"){
        var imgUrl = "https://www.singaporeair.com/saar5/images/navigation/flying-withus/our-fleet/boeing-777-300er.jpg";
        var videoUrl = "https://www.youtube.com/watch?v=dAFHH3seBBU";
        var title =  "Our People";
        var subtitle =  "Find out what our people think."
        botSendTextMessage(sender, aiText);
        botSendVideo(sender, title, subtitle, imgUrl, videoUrl);
    }else if(response.result.action == "answer_reason_to_join"){
        botSendTextMessage(sender, aiText);
        botSendImage(sender, "https://www.singaporeair.com/saar5/images/careers/Why_Join_SIA/professional_development_2.jpg");
    }else if(response.result.action == "applied-for-job"){
        var job = response.result.parameters.job;
        doApply(sender,job);
    }else if(response.result.action == "answer_if_height_is_ok"){
        var height;
        var unitVal = "cm";
        if(response.result.parameters.number){
            height = response.result.parameters.number;
            if(height <= 3){
                unitVal = "m";
            }
        }else if(response.result.parameters['unit-length']){
            height = parseFloat(response.result.parameters['unit-length'].amount);
            unitVal = response.result.parameters['unit-length'].unit;
        }
        console.log("HEIGHT=" + height + ", UNIT=" + unitVal);
        if(response.result.parameters.gender == "female"){
          if(unitVal == "m" && height >= 1.58 || unitVal != "m" && height >= 158 ){
            aiText = "Congratulations! You have passed the height requirement! :)";
          }
        }else{
          if(unitVal == "m" && height >= 1.65 || unitVal != "m" && height >= 165 ){
            aiText = "Congratulations! You have passed the height requirement! :)";
          }
        }
        botSendTextMessage(sender, aiText);
    }else{
        botSendTextMessage(sender, aiText);
    }
  });

  apiai.on('error', (error) => {
    console.log(error);
  });

  apiai.end();
}

function botSendList(sender, elements, aiText){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: ACCESS_TOKEN },
    method: 'POST',
    json: {
      recipient: {id: sender},
      message:
      {
        attachment:
        {
          type: "template",
          payload: {
            template_type: "generic",
            elements: elements,
          }
        }
      }
    }
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    }
  });
}

function botSendVideo(sender, title, subtitle, imgUrl, videoUrl){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: ACCESS_TOKEN},
    method: 'POST',
    json: {
      recipient: {id: sender},
      "message":{
        "attachment":{
          "type":"template",
          "payload":{
            "template_type":"generic",
            "elements":[
               {
                "title": title,
                "image_url": imgUrl,
                "subtitle": subtitle,
                "default_action": {
                  "type": "web_url",
                  "url": videoUrl,
                  "messenger_extensions": true,
                  "webview_height_ratio": "tall",
                },
                "buttons":[
                  {
                    "type":"web_url",
                    "url": videoUrl,
                    "title":"Watch Video"
                  }
                ]
              }
            ]
          }
        }
      }
    }
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    } else{
        Database.insertMessage(sender,videoUrl,'B');
    }
  });
}

function botSendVideo2(sender, videoUrl){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: ACCESS_TOKEN},
    method: 'POST',
    json: {
      recipient: {id: sender},
      "message":{
       "attachment":{
         "type":"template",
         "payload":{
           "template_type":"open_graph",
           "elements":[
              {"url": videoUrl}
            ]
         }
       }
     }
   }
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    }else{
        Database.insertMessage(sender,videoUrl,'B');
    }
  });
}

function botSendImage(sender, imgUrl){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: ACCESS_TOKEN},
    method: 'POST',
    json: {
      recipient: {id: sender},
      "message":{
       "attachment":{
         "type":"image",
         "payload":{
           "url": imgUrl,
           "is_reusable":true
        }
       }
     }
   }
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    }else{
        Database.insertMessage(sender,imgUrl,'B');
    }
  });
}

function botSendTextMessage(sender, aiText){
  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {access_token: ACCESS_TOKEN},
    method: 'POST',
    json: {
      recipient: {id: sender},
      message: {text: aiText}
    }
  }, (error, response) => {
    if (error) {
        console.log('Error sending message: ', error);
    } else if (response.body.error) {
        console.log('Error: ', response.body.error);
    } else{
       Database.insertMessage(sender,aiText,'B');
    }
  });
}
